import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavbar from './Components/AppNavbar'

import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>
);

// const name = "Loren Dela Cruz"

// const student = {
//   firstName: "Maria",
//   lastName: "Dela Cruz"
// }

// function userName(user) {
//   return user.firstName + ' ' + user.lastName
// }

// const element = <h1>Hello, {userName(student)}</h1>

// root.render(element)