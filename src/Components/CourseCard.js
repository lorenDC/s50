import { Row, Col, Card, Button } from 'react-bootstrap'

export default function CourseCard() {
	return (
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="frontEnd">
			      <Card.Header as="h5">Front-End Development</Card.Header>
			      <Card.Body>
			        <Card.Title>HTML Course</Card.Title>
			        <Card.Text>
			          This course has everything you need to learn how write HTML code from scratch. Fast paced get to the point training, to learn about creating websites and using HTML code. By the end of the course you will have the skills and know how to apply HTML to make a real website.
			        </Card.Text>
			        <Button variant="primary">Learn More</Button>
			      </Card.Body>
			    </Card>
	    	</Col>
	    </Row>
		)
}