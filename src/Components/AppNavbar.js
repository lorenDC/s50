// old syntax to import
// import Navbar from 'react-bootstrap/NavBar'
// import Nav from 'react-bootstrap/Nav'

// new syntax to import
import { Navbar, Nav, Container } from 'react-bootstrap'

export default function AppNavbar() {
	return (
	 <Navbar bg="light" expand="md">
      <Container fluid>
        <Navbar.Brand href="#home">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav className="ml-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#courses">Courses</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
};

