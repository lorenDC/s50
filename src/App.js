import { Fragment } from 'react'
import { Container } from 'react-bootstrap'
import AppNavbar from './Components/AppNavbar'
// import Banner from './Components/Banner'
// import Highlights from './Components/Highlights'
import Home from './pages/Home'
import './App.css';

function App() {
  return (
    <Fragment>
      <AppNavbar/>
        <Container>
          <Home/>
        </Container>
    </Fragment>
  );
}

export default App;
