import { Fragment } from 'react'
import Banner from '../Components/Banner'
import Highlights from '../Components/Highlights'
import CourseCard from '../Components/CourseCard'
import Resource from '../Components/Resource'

export default function Home() {
	return (
		<Fragment>
			<Banner/>
			<Highlights/>
			<CourseCard/>
			<Resource/>
		</Fragment>
	)
}